package order

import (
	"time"
)

func ProcessingInit() {
	for {
		time.Sleep(200 * time.Millisecond)
		ri := RandomPoolItem(ARPoolLength-1, 0)
		// ReqQueue.mutex.Lock()
		if ReqQueue.ActiveReq[ri].Count > 0 {
			ReqQueue.InactiveReq = append(ReqQueue.InactiveReq, ReqQueue.ActiveReq[ri])
		}
		ReqQueue.ActiveReq[ri] = &Request{ID: RandomRequestID(RLen), Count: 0}
		// ReqQueue.mutex.Unlock()
	}
}

func GetRequestID() string {
	ri := RandomPoolItem(ARPoolLength-1, 0)
	// ReqQueue.mutex.Lock()
	// defer ReqQueue.mutex.Unlock()
	ReqQueue.ActiveReq[ri].Count++
	return ReqQueue.ActiveReq[ri].ID
}

//TODO: check is request already created
//isOrderCreated check that order name is already created
// func isOrderCreated(n string) bool {
// }

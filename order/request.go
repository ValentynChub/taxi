package order

import (
	"log"
	"sync"
)

var (
	RLen         int //length of request ID symbols
	ARPoolLength int // active orders pool length
	ReqQueue     Pool
)

type Pool struct {
	ActiveReq   []*Request //slice of Active requests
	InactiveReq []*Request //slice of Inactive requests
	mutex       sync.Mutex
}
type Request struct {
	ID    string `json:"ID"`
	Count int    `json:"count"`
}

// Init pool of requests initialization
func Init() {

	log.Printf("[INFO] Generating Orders Pool with length %v\n", ARPoolLength)
	for i := 0; i < ARPoolLength; i++ {
		rID := RandomRequestID(RLen)
		ReqQueue.ActiveReq = append(ReqQueue.ActiveReq, &Request{ID: rID, Count: 0})
	}
}

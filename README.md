Concurrency Level:      4000 </br>
Time taken for tests:   1.803 seconds</br>
Complete requests:      100000</br>
Failed requests:        0</br>
Keep-Alive requests:    100000</br>
Total transferred:      14200000 bytes</br>
HTML transferred:       200000 bytes</br>
Requests per second:    55472.62 [#/sec] (mean)</br>
Time per request:       72.108 [ms] (mean)</br>
Time per request:       0.018 [ms] (mean, across all concurrent requests)</br>
Transfer rate:          7692.49 [Kbytes/sec] received</br>

package main

import (
	"math/rand"
	"taxi/api"
	"taxi/order"
	"time"
)

func init() {
	rand.Seed(time.Now().UnixNano())
}

func main() {

	order.ARPoolLength = 50
	order.RLen = 2
	order.Init()
	go order.ProcessingInit()
	api.Init()
}

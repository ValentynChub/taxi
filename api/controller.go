package api

import "taxi/order"

type StatData struct {
	Active   []*order.Request `json:"Active"`
	Inactive []*order.Request `json:"Inactive"`
}

func getRequestID() string {
	return order.GetRequestID()
}

func getStat() *StatData {

	sd := &StatData{}

	for _, req := range order.ReqQueue.ActiveReq {
		if req.Count != 0 {
			a := &order.Request{ID: req.ID, Count: req.Count}
			sd.Active = append(sd.Active, a)
		}
	}
	for _, req := range order.ReqQueue.InactiveReq {
		if req.Count != 0 {
			a := &order.Request{ID: req.ID, Count: req.Count}
			sd.Inactive = append(sd.Inactive, a)
		}
	}
	return sd
}

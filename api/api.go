package api

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"

	"github.com/julienschmidt/httprouter"
)

var (
	wport string = "8085" //default port
)

func Init() {

	router := httprouter.New()
	router.GET("/", ping)
	router.GET("/request", req)
	router.GET("/admin/requests", stat)

	if err := http.ListenAndServe(GetPort(), router); err != nil {
		log.Panic(err)
	}
}

// Get the Port from the environment so we can run on Heroku
func GetPort() string {
	var port = os.Getenv("PORT")
	// Set a default port if there is nothing in the environment
	if port == "" {
		port = wport
		log.Printf("[INFO] No PORT environment variable detected, defaulting to %v \n", port)
	}
	return ":" + port
}

func ping(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	fmt.Fprintf(w, "pong")
}
func req(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	rID := getRequestID()
	fmt.Fprintf(w, rID)
}
func stat(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	gs := getStat()

	js, err := json.Marshal(gs)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.Write(js)
}
